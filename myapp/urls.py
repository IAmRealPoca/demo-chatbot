from django.urls import include, path
from rest_framework import routers
from . import views
from myapp.views import ChatbotViewSet

router = routers.DefaultRouter()
router.register(r'hero', views.HeroViewSet)
# router.register(r'chatbot', views.ChatbotViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # path('chatbot', ChatbotViewSet),
    path('chatbotreq/', views.mein_get),
    path('chatbotgreeting/', views.chatbot_greeting),
]