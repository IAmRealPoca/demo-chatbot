# from myapp.models import ChatbotModel
import random

# def read_data_to_sentence_and_word():
#     f = open('chatbot.txt', 'r', errors='ignore')
#     raw = f.read()
#     raw = raw.lower()

#     nltk.download('punkt')
#     nltk.download('wordnet')

#     sent_token = nltk.sent_tokenize(raw) #sentence tokens
#     word_token = nltk.word_tokenize(raw)
#     return [sent_token, word_token]

# def normalize_token():
#     lemmer = nltk.stem.WordNetLemmatizer()
#     lem_token = [lemmer.lemmatize(token) for token in tokens]

GREETING_INPUTS = ("hello", "hi", "greetings", "sup", "what's up","hey",)
GREETING_RESPONSES = ["hi", "hey", "*nods*", "hi there", "hello", "I am glad! You are talking to me"]

def simple_greeting(sentence):
    for word in sentence.split():
        if word.lower() in GREETING_INPUTS:
            return random.choice(GREETING_RESPONSES)