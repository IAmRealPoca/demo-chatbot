from django.db import models

# Create your models here.

class Hero(models.Model):
    name = models.CharField(max_length=60)
    alias = models.CharField(max_length=60)

    def __str__(self):
        return self.name

class ChatbotModel(models.Model):
    title = models.CharField(max_length=60)
    response = models.CharField(max_length=500)

    def __str__(self):
        return self.title

class ChatbotRequestModel(models.Model):
    customer = models.CharField(max_length=60)
    content = models.CharField(max_length=500)

    def __str__(self):
        return self.customer