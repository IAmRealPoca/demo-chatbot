from django.contrib import admin
from .models import Hero
from .models import ChatbotModel
from .models import ChatbotRequestModel

# Register your models here.

admin.site.register(Hero)
admin.site.register(ChatbotModel)
admin.site.register(ChatbotRequestModel)