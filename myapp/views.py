from django.shortcuts import render

from rest_framework import viewsets
from rest_framework import views
from rest_framework.response import Response
from rest_framework import status

from rest_framework.decorators import api_view

from .serializers import HeroSerializer
from .serializers import ChatbotModelSerializer
from .serializers import ChatbotRequestSerializer

from .models import Hero
from .models import ChatbotModel
from .models import ChatbotRequestModel

import myapp.my_logic.my_logic as my_logic
# Create your views here.

class HeroViewSet(viewsets.ModelViewSet):
    queryset = Hero.objects.all().order_by('name')
    serializer_class = HeroSerializer

class ChatbotViewSet(viewsets.ModelViewSet):
    queryset = ChatbotModel.objects.all().order_by('title')
    serializer_class = ChatbotModelSerializer

@api_view(['GET', 'POST'])
def mein_get(request):
    if request.method == 'GET':
        chatbot_request_dat = ChatbotRequestModel.objects.all()
        serializer = ChatbotRequestSerializer(chatbot_request_dat, many=True)
        return Response(serializer.data)
    if request.method == 'POST':
        request_data = request.data
        serialized_data = ChatbotRequestSerializer(data=request_data)
        if serialized_data.is_valid():
            serialized_data.save()
            return Response(serialized_data.data, status = status.HTTP_201_CREATED)
        
@api_view(['GET', 'POST'])
def chatbot_greeting(request):
    if request.method == 'POST':
        customer_request_data = request.data
        serialized_customer_request = ChatbotRequestSerializer(data = customer_request_data) #parse request json to ChatbotRequestObject
        if serialized_customer_request.is_valid():
            # serialized_customer_request.save()
            # for serialized_customer_request_item in serialized_customer_request.data:
            #     print(serialized_customer_request_item)
            print(serialized_customer_request.data.get('content'))
            response_content = my_logic.simple_greeting(serialized_customer_request.data.get('content'))
            title = "Greeting back"
            chatbot_response_object = ChatbotModel(title=title, response=response_content)
            chatbot_response_object_list = [chatbot_response_object]
            # chatbot_response_object.title = title
            # chatbot_response_object.response = response_content
            serializer = ChatbotModelSerializer(chatbot_response_object_list, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)