from rest_framework import serializers
from .models import Hero
from .models import ChatbotModel
from .models import ChatbotRequestModel

class HeroSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Hero
        fields = ('name', 'alias')

class ChatbotModelSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ChatbotModel
        fields = ('title', 'response')

class ChatbotRequestSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ChatbotRequestModel
        fields = ('customer', 'content')

